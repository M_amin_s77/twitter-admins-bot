<?php

class Bot
{

    var $botToken;
    var $botUrl;

    function __construct()
    {
        $this->botToken = "509770468:AAFJmSwmKkKnjYPGctDv3KsAUnKR5r-XNio";
        $this->botUrl = "https://api.telegram.org/bot" . $this->botToken;
    }

    function sendMessage($text, $chatId)
    {
        file_get_contents($this->botUrl . "/sendmessage?chat_id=" . $chatId . "&text=" . $text);
    }

    function sendReplyKeyboard($text, $chatId, $keyboardsValue)
    {
        $replyMarkup = array(
            'keyboard' => $keyboardsValue,
            'force_reply' => true,
            'selective' => true
        );
        $encodedMarkup = json_encode($replyMarkup, true);
        file_get_contents($this->botUrl . "/sendmessage?chat_id=" . $chatId . "&text=" . $text .
            "&reply_markup=" . $encodedMarkup);
    }

    function removeReplyKeyboard($text, $chatId)
    {
        file_get_contents($this->botUrl . "/sendmessage?chat_id=" . $chatId . "&text=" . $text .
            "&reply_markup={\"remove_keyboard\":true}");
    }
}


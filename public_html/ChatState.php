<?php
class ChatState{
    const NAMES = array("ASSIGN_SELECT_TASK", "ASSIGN_SELECT_USER");
    const ASSIGN_SELECT_TASK = 0;
    const ASSIGN_SELECT_USER = 1;

    static function getName($number){
        return ChatState::NAMES[$number];
    }

    static function getFromName($name){
        $index = array_search($name, ChatState::NAMES);
        return $index;
    }
}
<?php

class Day
{
    const NUMBER = 7;
    const SHANBE = 0;
    const YEKSHANBE = 1;
    const DOSHANBE = 2;
    const SESHANBE = 3;
    const CHAHARSHANBE = 4;
    const PANJSHANBE = 5;
    const JOME = 6;

    static function getName($number)
    {
        switch ($number) {
            case Day::SHANBE:
                return "SHANBE";
            case Day::YEKSHANBE:
                return "YEKSHANBE";
            case Day::DOSHANBE:
                return "DOSHANBE";
            case Day::SESHANBE:
                return "SESHANBE";
            case Day::CHAHARSHANBE:
                return "CHAHARSHANBE";
            case Day::PANJSHANBE:
                return "PANJSHANBE";
            case Day::JOME:
                return "JOME";
            default:
                return "NONE";
        }
    }

    static function getText($number)
    {
        switch ($number) {
            case Day::SHANBE:
                return "شنبه";
            case Day::YEKSHANBE:
                return "یکشنبه";
            case Day::DOSHANBE:
                return "دوشنبه";
            case Day::SESHANBE:
                return "سه‌شنبه";
            case Day::CHAHARSHANBE:
                return "چهار‌شنبه";
            case Day::PANJSHANBE:
                return "پنج‌شنبه";
            case Day::JOME:
                return "جمعه";
            default:
                return "غلط";
        }
    }
}
<?php
include_once "connectDb.php";
include_once "Day.php";
include_once "Work.php";
class Reminder extends SimpleOrm{

    static function addReminder($day, $time, $work){
        $reminder = new Reminder;
        $reminder->day = $day;
        $reminder->time = $time;
        $reminder->work = $work;
        $reminder->save();
    }

    function toString(){
        $result = "";
        $result = $result."reminder".$this->id.": ".Day::getText($this->day)
            ." , ".$this->time." , ".Work::getText($this->work);
        return $result;
    }
}
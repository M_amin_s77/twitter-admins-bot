<?php
include_once "connectDb.php";

class Session extends SimpleOrm
{
    static function addSession($key, $value, $chatId)
    {
        if (self::hasSession($key, $chatId)) {
            $session = self::getSession($key, $chatId);
            $session->keey = $key;
            $session->value = $value;
            $session->date = date("Y-m-d H:i:s");
            $session->chat_id = $chatId;
            $session->save();
        } else {
            $session = new Session;
            $session->keey = $key;
            $session->value = $value;
            $session->date = date("Y-m-d H:i:s");
            $session->chat_id = $chatId;
            $session->save();
        }
    }

    static function hasSession($key, $chatId)
    {
        $session = Session::sql("SELECT * FROM :table WHERE keey=$key AND chat_id = $chatId",
            SimpleOrm::FETCH_ONE);
        return ($session != null);
    }

    static function getSession($key, $chatId)
    {
        $session = Session::sql("SELECT * FROM :table WHERE keey=$key AND chat_id = $chatId",
            SimpleOrm::FETCH_ONE);
        return $session;
    }

    static function deleteChatSessions($chatId)
    {
        $sessions = Session::retrieveByChat_id($chatId, SimpleOrm::FETCH_MANY);
        foreach ($sessions as $session) {
            $session->delete();
        }
    }

    function toString(){
        return "key:".$this->keey." value:".$this->value;
    }
}
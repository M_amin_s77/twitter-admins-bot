<?php
include_once "connectDb.php";

class Task extends SimpleOrm
{
    static function addTask($day, $work, $user)
    {
        $task = new Task;
        $task->day = $day;
        $task->work = $work;
        $task->user = $user;
        $task->save();
    }

    function toString(){
        $result = "";
        $result = $result."task".$this->id;
        $result = $result.": ".Work::getText($this->work)." , ".Day::getText($this->day);
        if ($this->user != "-1")
            $result = $result." , ".$this->user;
        return $result;
    }
}

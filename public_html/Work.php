<?php

class Work
{
    const NUMBER = 2;
    const GROUP = 0;
    const CHANNEL = 1;

    static function getName($number)
    {
        switch ($number) {
            case Work::GROUP:
                return "GROUP";
            case Work::CHANNEL:
                return "CHANNEL";
        }
    }

    static function getText($number)
    {
        switch ($number) {
            case Work::GROUP:
                return "گذاشتن پست در گروه";
            case Work::CHANNEL:
                return "گذاشتن پست در کانال";
        }
    }
}
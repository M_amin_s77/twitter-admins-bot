<?php
include_once "Bot.php";
$bot = new Bot();
$content = file_get_contents("php://input");
$update = json_decode($content, TRUE);
$message = $update["message"];
$chatId = $message["chat"]["id"];
$text = $message["text"];
//include_once "Session.php";
//$sessions = Session::retrieveByChat_id($chatId, SimpleOrm::FETCH_MANY);
//foreach ($sessions as $session) {
//    $bot->sendMessage($session->toString(), $chatId);
//}


include_once  "main.php";
try {
    $main = new main($chatId, $text);
    $main->run();
}catch (\Exception $e){
    $bot->sendMessage($e->getMessage(), $chatId);
    $bot->sendMessage(urlencode($e->getTraceAsString()), $chatId);
}catch (Throwable $e){
    $bot->sendMessage($e->getMessage(), $chatId);
    $bot->sendMessage(urlencode($e->getTraceAsString()), $chatId);
}
?>
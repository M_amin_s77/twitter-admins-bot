<?php
include_once "Bot.php";
include_once "Day.php";
include_once "Reminder.php";
include_once "Work.php";
include_once "Task.php";
date_default_timezone_set("Iran");
$CHAT_ID = 104342322;
$bot = new Bot();
$day_num = date('N');
$day_num -= 6;
if ($day_num < 0)
    $day_num += 7;
$reminders = Reminder::retrieveByDay($day_num, SimpleOrm::FETCH_MANY);
foreach ($reminders as $reminder){
    $time = $reminder->time;
    $from_time = strtotime(date("Y-m-d")." ".$time);
    $to_time = strtotime(date("Y-m-d H:i:s"));
    $diff = round(abs($to_time - $from_time) / 60,2);
    if ($diff < 10){
        $task = Task::sql("Select * From :table WHERE day = $reminder->day AND work = $reminder->work",
            SimpleOrm::FETCH_ONE);
        if ($task->user != -1) {
            $bot->sendMessage("یادآوری:", $CHAT_ID);
            $bot->sendMessage($task->user . " , ".Work::getText($task->work), $CHAT_ID);
        }else{
            $bot->sendMessage("برای کار ِ".Work::getText($task->work)." فردی در نظر گرفته نشده است.", $CHAT_ID);
        }
    }
}

<?php
include_once "Bot.php";
include_once "Task.php";
include_once "Day.php";
include_once "Work.php";
include_once "SessionKey.php";
include_once "ChatState.php";
include_once "Session.php";
include_once "Reminder.php";
class main
{
    var $chatId;
    var $text;
    var $bot;
    const USERNAMES = array("@Amir_s98", "@Amin_Salarkia", "@MhmmdRza", "@AliRezaChief");

    public function __construct($chatId, $text)
    {
        $this->bot = new Bot();
        $this->chatId = $chatId;
        $this->text = $text;
    }

    function run()
    {
        if ($this->text == "/getnotassignedtasks") {
            Session::deleteChatSessions($this->chatId);
            $this->getNotSetTasks();
        } else if ($this->text == "/assigntask") {
            Session::deleteChatSessions($this->chatId);
            $this->assignTask();
        } else if($this->text == "/seeremindtimes"){
            Session::deleteChatSessions($this->chatId);
            $this->seeRemindTimes();
        } else if (Session::hasSession(SessionKey::CHAT_STATE, $this->chatId)) {
            $chatState = Session::getSession(SessionKey::CHAT_STATE, $this->chatId)->value;
            $number = ChatState::getFromName($chatState);
            switch ($number) {
                case ChatState::ASSIGN_SELECT_TASK:
                    $this->assignSelectTask();
                    break;
                case ChatState::ASSIGN_SELECT_USER:
                    $this->assignSelectUser();
                    break;
            }
        }

    }

    function seeRemindTimes(){
        $result = "";
        $reminders = Reminder::all();
        foreach ($reminders as $reminder){
            $result = $result.$reminder->toString().PHP_EOL;
        }
        $this->bot->removeReplyKeyboard(urlencode($result), $this->chatId);
    }

    function getNotSetTasks()
    {
        $result = "";
        $tasks = Task::retrieveByUser("-1", SimpleOrm::FETCH_MANY);
        for ($i = 0; $i < sizeof($tasks); $i++) {
            $result = $result . $tasks[$i]->toString() . PHP_EOL;
        }
        $this->bot->removeReplyKeyboard(urlencode($result), $this->chatId);
    }

    function assignTask()
    {
        $tasks = Task::all();
        $ids = array();
        for ($i = 0; $i < sizeof($tasks); $i++) {
            $ids[$i] = $tasks[$i]->id;
        }
        $this->bot->sendReplyKeyboard("لطفا شماره کاری که قصد اساین کردنِ آن را دارید وارد کنید.",
            $this->chatId, array($ids));
        Session::addSession(SessionKey::CHAT_STATE,
            ChatState::getName(ChatState::ASSIGN_SELECT_TASK),
            $this->chatId);
    }

    function assignSelectTask()
    {
        $num = filter_var($this->text, FILTER_VALIDATE_INT);
        if ($num !== false) {
            $this->bot->sendReplyKeyboard("لطفا نام کاربریِ کسی که قرار است به آن اساین شود را وارد کنید.",
                $this->chatId, array(main::USERNAMES));
            Session::addSession(SessionKey::CHAT_STATE,
                ChatState::getName(ChatState::ASSIGN_SELECT_USER), $this->chatId);
            Session::addSession(SessionKey::TASK_ID, (string)$num, $this->chatId);
        } else {
            $this->bot->sendMessage("شماره معتبر نمیباشد", $this->chatId);
        }
    }

    function assignSelectUser(){
        if (in_array($this->text, main::USERNAMES)){
            $this->bot->removeReplyKeyboard("تسک اساین شد.", $this->chatId);
            $task = Task::retrieveById((int)Session::getSession(SessionKey::TASK_ID, $this->chatId)->value,
                SimpleOrm::FETCH_ONE);
            $task->user = $this->text;
            $task->save();
            Session::deleteChatSessions($this->chatId);
        }else{
            $this->bot->sendMessage("نامِ کاربری معتبر نمیباشد.", $this->chatId);
        }
    }
}